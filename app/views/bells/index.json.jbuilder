json.array!(@bells) do |bell|
  json.extract! bell, :id, :name, :address
  json.url bell_url(bell, format: :json)
end
