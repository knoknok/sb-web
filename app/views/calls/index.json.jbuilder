json.array!(@calls) do |call|
  json.extract! call, :id, :bell_id, :action, :action_msg
  json.url call_url(call, format: :json)
end
