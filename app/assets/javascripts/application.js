// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require framework7
// require jquery
// require jquery_ujs
//= require turbolinks
//= require_tree .

/////////////
// GLOBAL //
///////////
var myApp = new Framework7({cache: false});
var $$ = Dom7;
var mainView = myApp.addView(".view-main");

////////////////
// BELL SHOW //
//////////////
function btnAtHomeClickEvent (e) {
	$$('#btn-at-home').addClass("active");
	$$('#btn-out').removeClass("active");
}

function btnOutClickEvent (e) {
	$$('#btn-out').addClass("active");
	$$('#btn-at-home').removeClass("active");
}

myApp.onPageInit('bell-show', function (page) {	
	$$('#btn-at-home').on('click', btnAtHomeClickEvent);
	$$('#btn-out').on('click', btnOutClickEvent);
});

// myApp.onPageBeforeRemove('bell-show', function (page) {
// 	$$('#btn-at-home').off('click', btnAtHomeClickEvent);
// 	$$('#btn-out').off('click', btnOutClickEvent);
// 	alert("off");
// });

////////////////
// CALL SHOW //
//////////////
var call_timer_sec = null;
var call_timer_handle = null;

myApp.onPageInit('call-show', function (page) {
	call_timer_sec = parseInt($$('#time-diff').val());

	function pad (val) {
		return val > 9 ? val : "0" + val;
	}

	call_timer_handle = setInterval( function(){
		$$("#seconds").html(pad(++call_timer_sec % 60));
		$$("#minutes").html(pad(parseInt(call_timer_sec / 60, 10)));
	}, 1000);
	
	$$('#action-come-up').on('click', function (e) {
		// TODO: Send message to device. Confirm if OK or KO.
		// If OK inactive call, go back to bell show
		// If KO, error message and stay on call show
		myApp.alert("OK or KO", "Come Up Action");
	});
	
	$$('#action-coming').on('click', function (e) {
		// TODO: Send message to device. Confirm if OK or KO.
		// If OK inactive call, go back to bell show
		// If KO, error message and stay on call show
		myApp.alert("OK or KO", "Coming Action");
	});
	
	$$('#action-coming-in').on('click', function (e) {
		// TODO: Send message to device. Confirm if OK or KO.
		// If OK inactive call, go back to bell show
		// If KO, error message and stay on call show
		myApp.prompt("How many minutes?", "Coming In Action");
	});
	
	$$('#action-cant').on('click', function (e) {
		// TODO: Send message to device. Confirm if OK or KO.
		// If OK inactive call, go back to bell show
		// If KO, error message and stay on call show
		myApp.alert("OK or KO", "Can't Make It Action");
	});
});

myApp.onPageBeforeRemove('call-show', function (page) {
	clearInterval(call_timer_handle);
	call_timer_sec = null;
	call_timer_handle = null;
});