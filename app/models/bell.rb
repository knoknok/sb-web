class Bell < ActiveRecord::Base
  has_many :calls
  
  def has_active_call?
    calls.size > 0 and calls.last.active
  end
  
  def active_call
    has_active_call? ? calls.last : nil
  end
end
