class AddActiveToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :active, :boolean
  end
end
