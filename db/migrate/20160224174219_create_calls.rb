class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.integer :bell_id
      t.integer :action
      t.string :action_msg

      t.timestamps null: false
    end
  end
end
