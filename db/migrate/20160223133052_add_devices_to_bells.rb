class AddDevicesToBells < ActiveRecord::Migration
  def change
    add_column :bells, :display_particle_id, :string
    add_column :bells, :bell_particle_id, :string
    add_column :bells, :particle_token, :string
  end
end
