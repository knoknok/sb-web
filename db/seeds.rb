# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Bell.create([{name: "Bourgoin Jallieu"},
            {name: "Vaulx-Milieu"},
            {name: "Ouagadougou"},
            {name: "Beirut"}])
            
Call.create([{bell_id: 1, active: true},
            {bell_id: 2, active: true}])